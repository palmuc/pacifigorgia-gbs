import ipyrad.analysis as ipa
import ipyparallel as ipp
import toytree
import toyplot

print(ipa.__version__)
print(toytree.__version__)
print(toyplot.__version__)

ipyclient = ipp.Client(cluster_id="baba")


locifile = "./paciGBS_50PctTaxa.loci"
newick = "./RAxML_bestTree.20PctGaps_PaciGBSGapped"

paciTree = toytree.tree(newick).root(wildcard="GW4696")

paciTree.get_tip_labels()
paciTree.is_rooted()
paciTree.draw()

bb = ipa.baba(data = locifile, newick = paciTree)

#yields 9 tests given the tree. All point to introgression between P1 and P3 => P. pumila and P. pumila clade
c1_constrain = {
    "p5": ["GW4696_Index_08_ACTTGA_sample_TTGTCA"],#L. taboguilla
    "p4": ["GW4653_Index_11_GGCTAC_sample_AACGTC", "GW4646_Index_12_CTTGTA_sample_AACGTC"],#P.stenobrochis clade
    "p3": ["GW4661_Index_09_GATCAG_sample_CCAAGT", "GW4671_Index_09_GATCAG_sample_AACGTC"],#P. pumila clade
    "p2": ["GW4675_Index_09_GATCAG_sample_GGTTCA"],#P. stenobrochis
}

#yields 21 tests given the tree. All indicate no introgression can be detected within this clade.
c2_constrain = {
    "p4": ["GW4696_Index_08_ACTTGA_sample_TTGTCA"],#L. taboguilla
    "p3": ["GW4689_Index_08_ACTTGA_sample_GGTCAC", "GW4637_Index_08_ACTTGA_sample_TTGCAC"],#P. rubicunda clade
    "p2": ["GW4638_Index_10_TAGCTT_sample_CCAGTA"],#P. eximia
}

#yields 51 tests given the tree.  All indicate no introgression can be detected within this clade.
c4_constrain = {
    "p4": ["GW4696_Index_08_ACTTGA_sample_TTGTCA"],#L. taboguilla
    "p3": ["GW4688_Index_11_GGCTAC_sample_TTGCAC", "GW4626_Index_11_GGCTAC_sample_GGTCAC"],#P. smithsoniana, P. rubicunda
    "p2": ["GW4628_Index_09_GATCAG_sample_AACAGT", "GW4630_Index_08_ACTTGA_sample_CCAGTA"],#P. ferruginea clade
}

#yields 15 tests given the tree. All point to introgression between P2 and P3 => P. stenobrochis and (P.stenobrochis, P. cairnsi)
c5_constrain = {
    "p5": ["GW4672_Index_10_TAGCTT_sample_GGTCAC", "GW4686_Index_10_TAGCTT_sample_TTGCAC"],#P. rubicunda, P. cairnsi
    "p4": ["GW4651_Index_12_CTTGTA_sample_CCAGTA", "GW4690_Index_11_GGCTAC_sample_CCAGTA", "GW4710_Index_11_GGCTAC_sample_CCAAGT"], #P. cairnsi, P. smithsoniana, P. cairnsi
    "p3": ["GW4655_Index_09_GATCAG_sample_TTGCAC", "GW4645_Index_09_GATCAG_sample_GGTCAC"],#P. cairnsi, P. stenobrochis
    "p2": ["GW4631_Index_11_GGCTAC_sample_TTGTCA"], #P. stenobrochis
}

#yields three tests given the tree. All indicate no introgression can be detected within this clade.
c6_constrain = {
    "p4": ["GW4696_Index_08_ACTTGA_sample_TTGTCA"],#L. taboguilla
    "p3": ["GW4695_Index_12_CTTGTA_sample_GGTCAC", "GW4648_Index_12_CTTGTA_sample_TTGCAC"],#P. rubicunda. P. firma
    "p2": ["GW4633_Index_08_ACTTGA_sample_AACAGT"],#P. ferruginea
}

#yields three tests given the tree. All indicate no introgression can be detected within this clade.
no_clade_constrain = {
    "p5": ["GW4696_Index_08_ACTTGA_sample_TTGTCA"],#L. taboguilla
    "p4": ["GW4662_Index_08_ACTTGA_sample_GGTTCA"],#P. cairnsi
    "p3": ["GW4649_Index_08_ACTTGA_sample_AACGTC"],#P. cairnsi
    "p2": ["GW4694_Index_10_TAGCTT_sample_CCAAGT", "GW4650_Index_12_CTTGTA_sample_CCAAGT"],#P. rubicunda clade
    "p1": ["GW4647_Index_12_CTTGTA_sample_GGTTCA", "GW4639_Indes_12_CTTGTA_sample_TTGTCA"]#P. stenobrochis, P. rubicunda
}

bb.generate_tests_from_tree(
    constraint_dict = no_clade_constrain
)

#bb.tests = [firma_ferruginea_constrain]
bb.run(ipyclient)

# In the figure:
#
# p4 is gray,
# p3 is black,
# p2 is orange,
# p1 is green.
# D > 0 => introgression between p2 and P3
# D = 0 => NO introgression
# D < 0 => introgression between p1 and P3

bb.plot(height = 1500, width = 900, pct_tree_y = 0.2, alpha=4)
