# pacifigorgia-GBS 

## Hybridization and cryptic speciation in the Tropical Eastern Pacific octocoral genus *Pacifigorgia*

Authors: Angelo Poliseno, Odalisca Breedy, Hector M. Guzman, and Sergio Vargas

The shallow waters of the Tropical Eastern Pacific (TEP) harbor a species-rich octocoral fauna, with seven genera and 124 octocoral species described to date for the region. Of these lineages, *Pacifigorgia*, with 35 species, is by far the most speciose and abundant shallow-water octocoral occurring in the region. The speciation mechanisms resulting in this remarkable diversity remain speculative, despite the extensive taxonomic and molecular systematic research conducted so far in the TEP. Using genome-wide SNP markers, we provide evidence for hybridization and extensive cryptic speciation in *Pacifigorgia*, suggesting that the genus’ diversity has been underestimated by traditional and molecular systematic research. Our study highlights the difficulties faced by both traditional taxonomy and single-marker based molecular approaches to characterize octocoral diversity and evolution, and the role genome-wide molecular studies coupled to morphological research play to advance our understanding of this group.

## Repository organization

**ABBA-BABA_Analyses**
                   |---> baba.py = (python) script used for abba-baba tests 
                   |---> paciGBS_50PctTaxa.loci = SNP data matrix used for ABBA-BABA Tests

**DAPC_Analyses**
              |---> paciGBS.R = (R) script used for DAPC analyses
              |---> paciGBS_50.vcf = SNP data matrix used for DAPC analyses
              |---> Pacifigorgia_DAPC.RData = RData file with DAPC analyses

**RAx_Analyses**
             |---> 20PctGaps_paciGBSGapped.phy = Alignment used for ML tree reconstruction
             |---> RAxML_* = RAxML results


#### NOTES

1. This repository (or parts of it) is in active development. Check the releases section to see if there are snap-shots of it.

2. This repository is **public** and the following personal information is visible:
	* After each **commit*, the name and E-mail address as saved in your Git config.
	* The **GitLab username* as saved in your user settings (under Account).

3. In German: Bitte beachten Sie: Bei öffentlichen Projekten sind die folgenden Informationen öffentlich sichtbar (allgemein zugreifbar):
	* Bei jedem Commit der Name und die E-Mail-Adresse, wie sie in der Konfiguration von Git hinterlegt wurden.
	* Bei GitLab der GitLab-Benutzername (Username), wie er bei den Benutzereinstellungen (User Settings) im Abschnitt "Konto" (Account) festgelegt wird. Der Benutzername ist bei persönlichen Projekten im Pfad sichtbar.

#### Note that by commiting to this repository you accept the publication of the information detailed above. More information about public repositories can be find here: https://doku.lrz.de/display/PUBLIC/GitLab


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />The files stored in this repository are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.